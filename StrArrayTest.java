import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * The test class StrArrayTest.
 *
 * @author  Barry Brown
 * @version 2019-09-25
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StrArrayTest
{
    /**
     * Default constructor for test class StrArrayTest
     */
    public StrArrayTest()
    {
    }

    @Test
    public void testIndex_1()
    {
        String[] s1 = {"alpha", "beta", "epsilon", "phi"};
        StrArray sa = new StrArray(s1);
        // The longest string is "epsilon" at index 2
        assertEquals(2, sa.indexOfLongest());
    }
    
    @Test
    public void testIndex_2()
    {
        String[] s2 = {"beta", "zeta", "phi", "gamma", "psi", "alpha"};
        StrArray sa = new StrArray(s2);
        // There are two longest strings, so return the index of the
        // first one ("gamma" at 3)
        assertEquals(3, sa.indexOfLongest());
    }
    
    @Test
    public void testIndex_3()
    {
        String[] s3 = {};
        StrArray sa = new StrArray(s3);
        // The empty array has no longest, so return -1
        assertEquals(-1, sa.indexOfLongest());
    }
    
    @Test
    public void testIndex_4()
    {
        String[] s4 = {"alpha"};
        StrArray sa = new StrArray(s4);
        // The longest string is "alpha" at position 0
        assertEquals(0, sa.indexOfLongest());
    }
    
    @Test
    public void testIndex_5()
    {
        String[] s5 = {"delta", "gamma"};
        StrArray sa = new StrArray(s5);
        // There are two longest strings, so return the index of
        // the first one
        assertEquals(0, sa.indexOfLongest());
    }
    
    @Test
    public void testContains_1()
    {
        String[] s6 = {"alpha", "beta", "epsilon", "beta"};
        StrArray sa = new StrArray(s6);
        assertTrue(sa.contains("alpha"));
        assertTrue(sa.contains("beta"));
        assertTrue(sa.contains("epsilon"));
        assertFalse(sa.contains("gamma"));
        assertFalse(sa.contains(null));
        assertFalse(sa.contains(""));
    }
}
