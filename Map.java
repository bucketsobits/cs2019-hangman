import java.util.HashMap;
import java.util.Set;
import java.util.ArrayList;

public class Map
{
    // Delcare a HashMap whose keys are Strings and values are
    // Integers.
    // This is public so the test class can get access to it.
    public HashMap map;
    
    /**
     * Initialize the HashMap. Store nothing in it for now.
     */
    public Map()
    {
        map = new HashMap();
    }
    
    /**
     * Add the following keys and values to the map. No need for a
     * loop; just add the key/value pairs.
     * 
     * key       value
     * Monday    88
     * Tuesday   96
     * Wednesday 98
     * Thursday  77
     * Friday    72
     */
    public void addEntries()
    {
        map.put("Monday",88);
        map.put("Tuesday",96);
        map.put("Wednesday",98);
        map.put("Thursday",77);
        map.put("Friday",72);
    }
    
    /**
     * Return the key whose value is the most (largest).
     * Do not assume the map contains the key/value pairs shown above.
     * The testing class will load different entries.
     */
    public String maxValue()
    {
        int largestValue = 0;
        String largestKey = "";
        //ArrayList list = new ArrayList();
        Set<String> keys = map.keySet();
        
        for(String k:keys)
        {
            if(largestValue < (int)map.get(k))
            {
                largestKey = k;
                largestValue = (int)map.get(k);
            }
        }
        return largestKey;
    }
    
    /*
     * String key = "";
        int largestValue = 0;
        ArrayList list = new ArrayList();
        
        Set<String> keys = map.keySet();
        
        
        
        for(int i = 0; i < map.size(); i++)
        {
            if(largestValue < (int)list.get(i))
            {
                
            }
        }
        return key;
     */
}