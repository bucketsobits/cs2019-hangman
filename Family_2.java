import java.util.ArrayList;

public class Family_2
{
    private String[] words;
    
    /**
     * No need to change the constructor.
     */
    public Family_2(String[] w)
    {
        words = w;
    }
    
    /**
     * Given a single character, return an ArrayList of
     * all the word families. Each family should
     * appear only once in the ArrayList and there should be none
     * that aren't needed. The returned list can be in any order.
     */
    public ArrayList<String> familiesOf(char c)
    {
        ArrayList<String>list;
        list = new ArrayList<String>();
        String current = "";
        String fam = "";
        for(int i = 0; i < words.length ; i++)
        {
            String currentWord = "";
            currentWord = words[i];
                for(int o = 0; o < currentWord.length(); o++)
                {
                    if(currentWord.charAt(o) == c)
                    {
                        fam = fam + c;
                    }
                    else{fam = fam + "-";}
                }
            
            
            if(list.contains(fam)){}
            else
            {
                list.add(fam);
            }
            fam = "";
        }
        return list;
    }
}   