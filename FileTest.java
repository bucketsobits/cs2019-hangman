import java.io.File;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.BufferedReader;

public class FileTest
{
    private String filename;
    public ArrayList<String> arr;
    
    public FileTest(String f)
    {
        filename = f;
        arr = new ArrayList<String>();
    }
    
    public void readFile()
    {
        File f = new File(filename);
        
        try
        {
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            
            String line;
            line = br.readLine();
            while (line != null)
            {
                arr.add(line);
                line = br.readLine();
            }
        }
        catch (java.io.FileNotFoundException fnfe)
        {
            System.out.println("File not found: " + filename);
            System.exit(1);  // Maybe this is not what you want to do
        }
        catch (java.io.IOException ioe)
        {
            System.out.println("Oops. Something bad.");
            System.exit(1);
        }
    }
    
    public void printArray()
    {
        System.out.println(arr);
    }
}