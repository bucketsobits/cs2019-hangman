public class Family
{
    /**
     * Given a word and a character, return the corresponding
     * family.
     * For example:
     *   whatFamily("fast", 'a')   ->  "-a--"
     *   whatFamily("look", 'o')   ->  "-oo-"
     *   whatFamily("total", 'b')  ->  "-----"
     * See the tests for more examples.
     */
    public String whatFamily(String word, char c)
    {
        String fam = "";
        
        for(int i = 0; i < word.length(); i++)
        {
            if(word.charAt(i) == c)
            {
                fam += c;
            }
            else{fam += "-";}
        }
        
        return fam;
    }
}