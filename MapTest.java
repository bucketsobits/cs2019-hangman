import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * The test class MapTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MapTest
{
    /**
     * Default constructor for test class MapTest
     */
    public MapTest()
    {
    }

    @Test
    public void testMap_1()
    {
        Map m = new Map();
        assertNotNull(m.map);
        assertEquals(0, m.map.size());
    }
    
    @Test
    public void testMap_2()
    {
        Map m = new Map();
        m.addEntries();
        assertEquals(5, m.map.size());
        assertTrue(m.map.containsKey("Monday"));
        assertTrue(m.map.containsKey("Tuesday"));
        assertTrue(m.map.containsKey("Wednesday"));
        assertTrue(m.map.containsKey("Thursday"));
        assertTrue(m.map.containsKey("Friday"));
        assertEquals(88, (int)m.map.get("Monday"));
        assertEquals(96, (int)m.map.get("Tuesday"));
        assertEquals(98, (int)m.map.get("Wednesday"));
        assertEquals(77, (int)m.map.get("Thursday"));
        assertEquals(72, (int)m.map.get("Friday"));
        }
    
    @Test
    public void testMap_3()
    {
        Map m = new Map();
        m.map.put("aardvark", 20);
        m.map.put("skunk", 10);
        m.map.put("cat", 12);
        m.map.put("dog", 15);
        assertEquals("aardvark", m.maxValue());
    }
}
