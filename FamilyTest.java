import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class FamilyTest.
 *
 * @author  Barry Brown
 * @version 2019-09-25
 */
public class FamilyTest
{
    /**
     * Default constructor for test class FamilyTest
     */
    public FamilyTest()
    {
    }

    @Test
    public void testWhatFamily()
    {
        Family f = new Family();
        assertEquals("-i--",  f.whatFamily("find", 'i'));
        assertEquals("f---",  f.whatFamily("find", 'f'));
        assertEquals("e--e",  f.whatFamily("else", 'e'));
        assertEquals("--ee-", f.whatFamily("sleep", 'e'));
        assertEquals("---",   f.whatFamily("lip", 'a'));
        assertEquals("-",     f.whatFamily("g", 'x'));
        assertEquals("t",     f.whatFamily("t", 't'));
        assertEquals("bbbbb", f.whatFamily("bbbbb", 'b'));
    }
}
