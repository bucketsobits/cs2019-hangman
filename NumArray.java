public class NumArray
{
    private int[] arr;
    
    /**
     * Constructor that stores away the given array. No need to
     * change this.
     */
    public NumArray(int[] a)
    {
        arr = a;
    }
    
    /**
     * Return the maximum (largest) value in the arr array.
     * You may assume the array consists of positive ints.
     * If there is no maximum, return -1.
     */
    public int max()
    {
        int largepos = 0;
        int ret = 0;
        if(arr.length > 0)
        {
            for(int i = 0; i < arr.length; i++)
            {
                if(arr[i] > arr[largepos])
                {
                    largepos = i;
                }
            }
            ret = arr[largepos];
        }
        else{ret = -1;}
        return ret;
    }
}