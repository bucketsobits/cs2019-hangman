public class StrArray
{
    private String[] arr;
    
    /**
     * Constructor that stores the given String array.
     * No need to change this.
     */
    public StrArray(String[] a)
    {
        arr = a;
    }
    
    /**
     * Return the index of the longest string. If there is more than one
     * longest, return the index of the first one.
     * If there is no longest, return -1;
     */
    public int indexOfLongest()
    {
        int largepos = 0;
        int ret = 0;
        if(arr.length > 0)
        {
            for(int i = 0; i < arr.length; i++)
            {
                if(arr[i].length() > arr[largepos].length())
                {
                    largepos = i;
                }
            }
            ret = largepos;
        }
        else{ret = -1;}
        return ret;
    }
    
    /**
     * Return true if the array contains the given String;
     * false otherwise.
     */
    public boolean contains(String target)
    {
        boolean test = false;
        for(int i = 0; i < arr.length; i++)
        {
            if(arr[i].equals(target))
            {
                test = true;
            }
        }
        return test;
    }
}