import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;

/**
 * The test class Family2Test.
 *
 * @author  Barry Brown
 * @version 2019-09-24
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Family2Test
{
    String[] list1;
    String[] list2;
    String[] list3;
    
    /**
     * Default constructor for test class HangmanTest
     */
    public Family2Test()
    {
        list1 = new String[] {"fish", "look", "flow", "fowl", "cool"};
        list2 = new String[] {"eel", "lee", "eee", "lll", "ele"};
        list3 = new String[] {"light", "cling", "floof", "elles"};
    }
    
    @Test
    public void testFamiliesOf_1()
    {
        Family_2 f = new Family_2(list1);
        ArrayList<String> ret = f.familiesOf('l');
        assertTrue(ret.contains("----"));
        assertTrue(ret.contains("l---"));
        assertTrue(ret.contains("---l"));
        assertTrue(ret.contains("-l--"));
        assertEquals(4, ret.size());
    }
    
    @Test
    public void testFamiliesOf_2()
    {
        Family_2 f = new Family_2(list1);
        ArrayList<String> ret = f.familiesOf('o');
        assertTrue(ret.contains("----"));
        assertTrue(ret.contains("-oo-"));
        assertTrue(ret.contains("-o--"));
        assertTrue(ret.contains("--o-"));
        assertEquals(4, ret.size());
    }
    
    @Test
    public void testFamiliesOf_3()
    {
        Family_2 f = new Family_2(list3);
        ArrayList<String> ret = f.familiesOf('l');
        assertTrue(ret.contains("l----"));
        assertTrue(ret.contains("-l---"));
        assertTrue(ret.contains("-ll--"));
        assertEquals(3, ret.size());
    }
    
    @Test
    public void testFamiliesOf_4()
    {
        Family_2 f = new Family_2(list3);
        ArrayList<String> ret = f.familiesOf('s');
        assertTrue(ret.contains("-----"));
        assertTrue(ret.contains("----s"));
        assertEquals(2, ret.size());
    }
    
    @Test
    public void testFamiliesOf_5()
    {
        Family_2 f = new Family_2(list2);
        ArrayList<String> ret = f.familiesOf('e');
        assertTrue(ret.contains("---"));
        assertTrue(ret.contains("eee"));
        assertTrue(ret.contains("ee-"));
        assertTrue(ret.contains("-ee"));
        assertTrue(ret.contains("e-e"));
        assertEquals(5, ret.size());
    }
    
    @Test
    public void testFamiliesOf_6()
    {
        Family_2 f = new Family_2(list2);
        ArrayList<String> ret = f.familiesOf('l');
        assertTrue(ret.contains("---"));
        assertTrue(ret.contains("l--"));
        assertTrue(ret.contains("--l"));
        assertTrue(ret.contains("-l-"));
        assertTrue(ret.contains("lll"));
        assertEquals(5, ret.size());
    }
    
    @Test
    public void testFamiliesOf_7()
    {
        Family_2 f = new Family_2(list3);
        ArrayList<String> ret = f.familiesOf('a');
        assertTrue(ret.contains("-----"));
        assertEquals(1, ret.size());
    }
    
    @Test
    public void testFamiliesOf_8()
    {
        Family_2 f = new Family_2(list1);
        ArrayList<String> ret = f.familiesOf('f');
        assertTrue(ret.contains("----"));
        assertTrue(ret.contains("f---"));
        assertEquals(2, ret.size());
    }
}
