import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 * The test class NumArrayTest.
 *
 * @author  Barry Brown
 * @version 2019-9-25
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NumArrayTest
{
    /**
     * Default constructor for test class NumArrayTest
     */
    public NumArrayTest()
    {
    }

    @Test
    public void testMax_1()
    {
        int[] a = {5, 3, 8, 3};
        NumArray n = new NumArray(a);
        assertEquals(8, n.max());
    }
    
    @Test
    public void testMax_2()
    {
        int[] a = {4, 6, 5, 4, 2, 5, 5};
        NumArray n = new NumArray(a);
        assertEquals(6, n.max());
    }
    
    @Test
    public void testMax_3()
    {
        int[] a = {3, 7, 4, 5, 7, 3};
        NumArray n = new NumArray(a);
        assertEquals(7, n.max());
    }
    
    @Test
    public void testMax_4()
    {
        int[] a = {};
        NumArray n = new NumArray(a);
        assertEquals(-1, n.max());
    }
    
    @Test
    public void testMax_5()
    {
        int[] a = {100};
        NumArray n = new NumArray(a);
        assertEquals(100, n.max());
    }
    
    @Test
    public void testMax_6()
    {
        int[] a = {50, 40, 30, 20, 10};
        NumArray n = new NumArray(a);
        assertEquals(50, n.max());
    }
}
