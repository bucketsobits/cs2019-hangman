import java.util.HashMap;
import java.util.Set;
import java.io.File;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.BufferedReader;

public class Hangman
{
    public String filename = "";
    public ArrayList<String> arr;
    public String family;
    public HashMap map;
    
    public char currentGuess = 'o';
    
    //Constructor
    public Hangman(String dictionaryName,int wordLength)
    {
        filename = dictionaryName;
        File f = new File(filename);
        
        
        for(int i = 0; i < wordLength;i++)
        {family += "-";}
        
        try
        {
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            
            int count = 0;
            
            String line;
            line = br.readLine();
            while (line != null)
            {
                if(line.length() == wordLength)
                {
                    arr.add( count , line );
                }
                line = br.readLine();
                count++;
            }
        }
        catch (java.io.FileNotFoundException fnfe)
        {
            System.out.println("File not found: " + filename);
            System.exit(1);  // Maybe this is not what you want to do
        }
        catch (java.io.IOException ioe)
        {
            System.out.println("whoops");
            System.exit(1);
        }
        
    }
    
    public String GetCurrentFamily()
    {
        return family;
    }
    
    public void MakeGuess(char g)
    {
        currentGuess = g;
        
        
        
        System.out.println("Current Family: " + GetCurrentFamily());
        System.out.println("Make Guess:");
    }
    
    public HashMap FamilyMap()
    {
        map = new HashMap();
        
        for(int i = 0; i < arr.size(); i++)
        {
            map.put( arr.get(i) , WordFamily( arr.get(i), currentGuess ) );
        }
        
        return map;
    }
    
    public int FamilyLength()
    {
        return family.length();
    }
    
    public String WordFamily(String word, char guess)
    {
        String fam = "";
        
        for(int i = 0; i < word.length(); i++)
        {
            if(word.charAt(i) == guess)
            {
                fam += guess;
            }
            else
            {
                fam += "-";
            }
        }
        
        return fam;
    }
}